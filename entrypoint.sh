#!/bin/sh

echo 'Running migrations...'
npx prisma migrate deploy --preview-feature

echo 'Checking if seeding is needed...'
npm run seed:dist

echo 'Starting server...'
npm run start
