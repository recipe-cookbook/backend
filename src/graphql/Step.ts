import { objectType } from 'nexus'

export const Step = objectType({
  name: 'Step',
  definition(t) {
    t.model.id()
    // t.model.createdAt()
    // t.model.updatedAt()
    t.model.description()
    t.model.recipeSection()
    t.model.order()
    t.model.photo()
  },
})
