import _ from 'lodash'
import { arg, list, mutationType, nonNull, stringArg } from 'nexus'

export const Mutation = mutationType({
  definition(t) {
    t.crud.createOneUser()
    t.crud.createOneIngredient()
    t.crud.createOneRecipe()
    t.crud.createOneRecipeSection()
    t.crud.createOneStep()
    t.crud.createOneTag()
    t.crud.createOneIngredientUnit()
    t.crud.createOneUnitSize()

    t.crud.updateOneRecipe()
    t.crud.updateOneIngredient()
    t.crud.updateOneIngredientUnit()
    t.crud.updateOneStep()
    t.crud.updateOneUnitSize()
    t.crud.updateOneRecipeSection()
    t.crud.updateOneTag()
    t.crud.updateOneUser()

    // t.crud.deleteOneRecipe()
    t.crud.deleteOneIngredient()
    t.crud.deleteOneIngredientUnit()
    t.crud.deleteOneRecipeSection()
    t.crud.deleteOneStep()
    t.crud.deleteOneTag()
    t.crud.deleteOneUnitSize()
    t.crud.deleteOneUser()

    // try this again later maybe after its typings are fixed.
    // t.crud.deleteOneRecipe({
    //   resolve: async (root, args, ctx, info, originalResolve) => {
    //     if (!args.where.id) {
    //       throw new Error(`Missing id in where clause`)
    //     }
    //     await ctx.prisma.step.deleteMany({
    //       where: {
    //         recipeSection: {
    //           recipe: {
    //             id: args.where.id,
    //           },
    //         },
    //       },
    //     })
    //     await ctx.prisma.ingredientUnit.deleteMany({
    //       where: {
    //         recipeSection: {
    //           recipe: {
    //             id: args.where.id,
    //           },
    //         },
    //       },
    //     })
    //     await ctx.prisma.recipeSection.deleteMany({
    //       where: {
    //         recipeId: args.where.id,
    //       },
    //     })
    //     const res = await originalResolve(root, args, ctx, info)

    //     return res
    //   },
    // })

    t.field('deleteOneRecipe', {
      type: 'Recipe',
      args: {
        where: nonNull(
          arg({
            type: 'RecipeWhereUniqueInput',
          }),
        ),
      },
      resolve: async (parent, args, ctx) => {
        if (!args.where.id) {
          throw new Error(`Missing id in where clause`)
        }
        await ctx.prisma.step.deleteMany({
          where: {
            recipeSection: {
              recipe: {
                id: args.where.id,
              },
            },
          },
        })
        await ctx.prisma.ingredientUnit.deleteMany({
          where: {
            recipeSection: {
              recipe: {
                id: args.where.id,
              },
            },
          },
        })
        await ctx.prisma.recipeSection.deleteMany({
          where: {
            recipeId: args.where.id,
          },
        })
        const recipe = await ctx.prisma.recipe.delete({
          where: { id: args.where.id },
        })

        return recipe
      },
    })

    t.nonNull.list.field('findOrCreateTags', {
      type: 'Tag',
      args: {
        names: nonNull(list(stringArg())),
      },
      resolve: async (parent, { names }: { names: (string | null)[] }, ctx) => {
        const nonNullNames = names.filter((x) => x) as string[]
        const foundTags = await ctx.prisma.tag.findMany({
          where: {
            name: {
              in: nonNullNames,
            },
          },
        })

        // 0 = found, 1 = not found
        const tags = _.partition(nonNullNames, (name) => {
          return foundTags.map((tag) => tag.name).includes(name)
        })

        const existingTags = foundTags.filter((tag) =>
          tags[0].includes(tag.name),
        )

        const createdTags = await Promise.all(
          tags[1].map((name) =>
            ctx.prisma.tag.create({
              data: { name },
            }),
          ),
        )

        return [...existingTags, ...createdTags]
      },
    })

    // make this more generic
    t.nonNull.list.field('findOrCreateIngredients', {
      type: 'Ingredient',
      args: {
        names: nonNull(list(stringArg())),
      },
      resolve: async (parent, { names }: { names: (string | null)[] }, ctx) => {
        const nonNullNames = names.filter((x) => x) as string[]
        const foundItems = await ctx.prisma.ingredient.findMany({
          where: {
            name: {
              in: nonNullNames,
            },
          },
        })

        // 0 = found, 1 = not found
        const items = _.partition(nonNullNames, (name) => {
          return foundItems.map((item) => item.name).includes(name)
        })

        const existingItems = foundItems.filter((item) =>
          items[0].includes(item.name),
        )

        const createdItems = await Promise.all(
          items[1].map((name) =>
            ctx.prisma.ingredient.create({
              data: { name },
            }),
          ),
        )

        return [...existingItems, ...createdItems]
      },
    })
  },
})
