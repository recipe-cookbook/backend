import { objectType } from 'nexus'

export const RecipeSection = objectType({
  name: 'RecipeSection',
  definition(t) {
    t.model.id()
    // t.model.createdAt()
    // t.model.updatedAt()
    t.model.cookTimeMinutes()
    t.model.description()
    t.model.ingredients({
      ordering: true,
    })
    t.model.name()
    t.model.prepTimeMinutes()
    t.model.recipe()
    t.model.servingUnit()
    t.model.servings()
    t.model.steps({
      ordering: true,
    })
    t.model.order()
  },
})
