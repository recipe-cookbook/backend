import { nonNull, queryType, stringArg } from 'nexus'
import { getUser } from '../util/auth'
import { getLogger } from '../util/logger'

const logger = getLogger({ subsystem: 'queries' })

export const Query = queryType({
  definition(t) {
    t.field('me', {
      type: 'User',
      resolve: async (parent, args, ctx) => {
        return getUser(ctx)
      },
    })

    t.crud.ingredientUnit()
    t.crud.ingredient()
    t.crud.user()
    t.crud.tag()
    t.crud.unitSize()
    t.crud.recipe()

    t.crud.step()
    t.crud.recipeSection()

    t.crud.recipeSections({ ordering: true })
    t.crud.ingredientUnits({
      ordering: true,
    })
    t.crud.ingredients({
      ordering: true,
      filtering: true,
    })
    t.crud.users({
      ordering: true,
    })
    t.crud.tags({
      ordering: true,
    })
    t.crud.unitSizes({
      ordering: true,
    })
    t.crud.recipes({
      ordering: true,
      filtering: true,
    })
    t.crud.steps({
      ordering: true,
    })

    t.list.field('filterRecipes', {
      type: 'Recipe',
      args: {
        searchString: nonNull(stringArg()),
      },
      resolve: (parent, { searchString }, ctx) => {
        return ctx.prisma.recipe.findMany({
          where: {
            OR: [
              {
                title: {
                  contains: searchString,
                },
              },
              {
                sections: {
                  some: {
                    ingredients: {
                      some: {
                        ingredient: {
                          name: {
                            contains: searchString,
                          },
                        },
                      },
                    },
                  },
                },
              },
              {
                tags: {
                  some: {
                    name: {
                      contains: searchString,
                    },
                  },
                },
              },
              {
                notes: {
                  contains: searchString,
                },
              },
            ],
          },
        })
      },
    })
  },
})
