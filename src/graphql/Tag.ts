import { objectType } from 'nexus'

export const Tag = objectType({
  name: 'Tag',
  definition(t) {
    t.model.id()
    // t.model.createdAt()
    // t.model.updatedAt()
    t.model.name()
    t.model.recipes()
  },
})
