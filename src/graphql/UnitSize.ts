import { objectType } from 'nexus'

export const UnitSize = objectType({
  name: 'UnitSize',
  definition(t) {
    t.model.id()
    // t.model.createdAt()
    // t.model.updatedAt()
    t.model.name()
    t.model.plural()
    t.model.unitType()
  },
})
