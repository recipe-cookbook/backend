import { objectType } from 'nexus'

export const File = objectType({
  name: 'File',
  definition(t) {
    t.model.id()
    // t.model.createdAt()
    // t.model.updatedAt()
    t.model.blob()
    t.model.mimetype()
    t.model.recipe()
    t.model.step()
    t.model.owner()
  },
})
