import { objectType } from 'nexus'

export const IngredientUnit = objectType({
  name: 'IngredientUnit',
  definition(t) {
    t.model.id()
    // t.model.createdAt()
    // t.model.updatedAt()
    t.model.amount()
    t.model.unit()
    t.model.notes()
    t.model.recipeSection()
    t.model.ingredient()
    t.model.order()
  },
})
