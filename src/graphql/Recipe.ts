import { objectType } from 'nexus'

export const Recipe = objectType({
  name: 'Recipe',
  definition(t) {
    t.model.id()
    t.model.createdAt()
    t.model.updatedAt()
    t.model.title()
    t.model.author()
    t.model.notes()
    t.model.sections({
      ordering: true,
    })
    t.model.description()
    t.model.source()
    t.model.tags()
    t.model.photo()
  },
})
