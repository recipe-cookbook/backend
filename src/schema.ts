import { DateTimeResolver } from 'graphql-scalars'
import { makeSchema } from 'nexus'
import { nexusPrisma } from '@kenchi/nexus-plugin-prisma'
import * as Path from 'path'
import * as types from './graphql'
import { getLogger } from './util/logger'

const logger = getLogger({ subsystem: 'schema' })
// logger.trace(`Loading schema library`)

export const schema = makeSchema({
  types,
  plugins: [
    nexusPrisma({
      experimentalCRUD: true,
      atomicOperations: false,
      scalars: {
        DateTime: DateTimeResolver,
        // Json: new GraphQLScalarType({
        //   ...JSONObjectResolver,
        //   name: 'Json',
        //   description:
        //     'The `JSON` scalar type represents JSON objects as specified by [ECMA-404](http://www.ecma-international.org/publications/files/ECMA-ST/ECMA-404.pdf).',
        // }),
      },
    }),
  ],
  outputs: {
    schema: __dirname + '/generated/schema.graphql',
    typegen: __dirname + '/generated/nexus.ts',
  },
  contextType: {
    module: Path.join(__dirname, './context.ts'),
    export: 'ContextModule',
  },
})
