import pino from 'pino'

export const logger = pino({
  level: 'trace',
})

export const getLogger = (
  props: pino.Bindings,
  opts?: pino.ChildLoggerOptions,
) => {
  return logger.child(props, opts)
}
