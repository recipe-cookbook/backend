import { User } from '@prisma/client'
import createFetch from '@turist/fetch'
import jwt from 'express-jwt'
import { Algorithm } from 'jsonwebtoken'
import jwksRsa from 'jwks-rsa'
import wretch from 'wretch'
import { dedupe, throttlingCache } from 'wretch-middlewares'
import { ContextModule } from '../context'
import { getLogger } from './logger'

const logger = getLogger({ subsystem: 'auth' })
// logger.trace(`Loading auth library`)

wretch()
  .middlewares([
    dedupe(),
    throttlingCache({
      throttle: 5 * 60 * 1000, // 5 minutes
    }),
  ])
  .polyfills({
    fetch: createFetch(),
    // FormData: require("form-data"),
    // URLSearchParams: require("url").URLSearchParams
  })

export interface Token {
  iss: string
  sub: string
  aud: string[]
  iat: number
  exp: number
  azp: string
  scope: string
  permissions: string[]
}

interface UserInfo {
  sub: string
  given_name: string
  family_name: string
  nickname: string
  name: string
  picture: string
  locale: string
  updated_at: string
  email: string
  email_verified: boolean
}

export interface UserWithPermissions extends User {
  permissions: string[]
}

const JWKS_URI = `${process.env.JWT_ISS?.replace(
  /\/$/,
  '',
)}/.well-known/jwks.json`

const USERINFO_URI = `${process.env.JWT_ISS?.replace(/\/$/, '')}/userinfo`

export const authMiddleware = jwt({
  // Dynamically provide a signing key based on the kid in the header and the signing keys provided by the JWKS endpoint.
  secret: jwksRsa.expressJwtSecret({
    cache: true,
    rateLimit: true,
    jwksRequestsPerMinute: 5,
    jwksUri: JWKS_URI,
  }),

  // Validate the audience and the issuer.
  audience: process.env.JWT_AUD,
  issuer: process.env.JWT_ISS,
  algorithms: (process.env.JWT_ALGS?.split(',') || ['RS256']) as Algorithm[],

  credentialsRequired: false,
})

export const getUser = async (
  context: ContextModule,
): Promise<UserWithPermissions> => {
  // logger.trace(`Called getUser`)
  const decodedToken = context.request.user
  if (!decodedToken) {
    throw new Error(`No user object passed to getUser context`)
  }
  const augmentPermissions = (
    user: User,
    permissions: string[],
  ): UserWithPermissions => {
    return {
      ...user,
      permissions,
    }
  }

  const lookupUserInfoFromRemote = async (authHeader: string) => {
    // logger.debug(`Looking up user from OAuth API`)
    const rawToken: string = authHeader.replace('Bearer ', '')

    const userInfo: UserInfo = await wretch(USERINFO_URI)
      .auth(`Bearer ${rawToken}`)
      .post()
      .json()

    return userInfo
  }

  const Authorization = context.request.get('Authorization')
  // logger.trace(`Getting authorization header`, { Authorization })
  if (Authorization) {
    const userFromSub = await context.prisma.jWTSub.findUnique({
      include: {
        user: {
          select: {
            email: true,
          },
        },
      },
      where: {
        sub: decodedToken.sub,
      },
    })

    // skip remote lookup if we have it locally
    const userInfo = !userFromSub
      ? await lookupUserInfoFromRemote(Authorization)
      : null

    const email = userFromSub ? userFromSub.user.email : userInfo?.email

    const user = await context.prisma.user.findUnique({
      where: {
        email: email,
      },
    })

    if (user) {
      if (!userFromSub) {
        logger.debug(
          `User existed but did not have a sub, attaching [${decodedToken.sub}]`,
        )
        await context.prisma.user.update({
          where: {
            id: user.id,
          },
          data: {
            jwtSub: {
              create: {
                sub: decodedToken.sub,
              },
            },
          },
        })
      }

      return augmentPermissions(user, decodedToken.permissions)
    } else if (userInfo) {
      logger.debug(
        `Creating new user ${userInfo.name} (${userInfo.email}) with sub [${decodedToken.sub}]`,
      )
      const newUser = await context.prisma.user.create({
        data: {
          email: userInfo.email,
          name: userInfo.name,
          jwtSub: {
            create: {
              sub: decodedToken.sub,
            },
          },
        },
      })

      return augmentPermissions(newUser, decodedToken.permissions)
    } else {
      throw new Error('Not Authorized')
    }
  } else {
    throw new Error('Not Authorized')
  }
}
