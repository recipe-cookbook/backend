import { Request, Response } from 'express-serve-static-core'
import { prisma } from '../prisma'
import { getUser, Token, UserWithPermissions } from './auth'
import { getLogger } from './logger'
import sharp from 'sharp'

const logger = getLogger({ subsystem: `file-handler` })

export const getFileHandler = async (req: Request, res: Response) => {
  const fileId = req.params.fileId

  const file = await prisma.file.findUnique({
    where: {
      id: fileId,
    },
  })

  if (!file) {
    return res.status(404).json({ error: `File not found` })
  }

  res
    .status(200)
    .header({
      'Content-Type': file.mimetype,
    })
    .send(file.blob)
}

export const deleteFileHandler = async (req: Request, res: Response) => {
  const fileId = req.params.fileId

  const file = await prisma.file.findUnique({
    where: {
      id: fileId,
    },
  })

  if (file) {
    await prisma.file.delete({
      where: {
        id: fileId,
      },
    })
  } else {
    return res.status(404).json({ error: `File not found` })
  }

  res.status(200).json({
    ok: true,
    message: `${fileId} deleted`,
  })
}

function isArray<T>(item: T | T[]): item is T[] {
  return (item as any).length !== undefined
}

export const postFileHandler = async (
  req: Request & { user?: Token },
  res: Response,
) => {
  let user: UserWithPermissions
  try {
    user = await getUser({
      prisma,
      request: req,
      log: logger,
      response: res,
    })
  } catch (ex) {
    logger.error(`Error with file upload routine: ${(ex as Error).message}`)
    res.status(400).json({ ok: false, error: `Not authorized or other error` })
    return
  }

  if (!user) {
    return res.status(404).json({ ok: false, error: `Not authorized` })
  }

  if (!req.files?.photo) {
    return res.status(404).json({ ok: false, error: `No photo data sent` })
  }

  const fileAsArray = await Promise.all(
    (isArray(req.files.photo) ? req.files.photo : [req.files.photo]).map(
      async (f) => sharp(f.data).resize(1280).webp({}).toBuffer(),
    ),
  )

  const fileId = req.params.fileId
  if (fileId) {
    // put request to replace
    const file = await prisma.file.findFirst({
      where: {
        id: fileId,
        owner: {
          id: user.id,
        },
      },
      select: {
        id: true,
      },
    })

    if (!file) {
      return res.status(404).json({ ok: false, error: `File not found` })
    }

    // update a file
    await prisma.file.update({
      where: {
        id: file.id,
      },
      data: {
        // mimetype: fileAsArray[0].mimetype,
        // blob: fileAsArray[0].data,
        mimetype: 'image/webp',
        blob: fileAsArray[0],
      },
    })
  } else {
    // post request to make new
    await prisma.file.createMany({
      data: fileAsArray.map((file) => ({
        // mimetype: file.mimetype,
        // blob: file.data,
        mimetype: 'image/webp',
        blob: file,
        recipeId: req.params.recipeId,
        userId: user.id,
      })),
    })
  }

  res.status(200).json({ ok: true, error: null })
}
