import { Prisma } from '@prisma/client'
import { and, or, rule, shield } from 'graphql-shield'
import { ContextModule } from '../context'
import { getUser, UserWithPermissions } from '../util/auth'

const isAdminUser = (user: UserWithPermissions) => {
  return (
    user.permissions.includes('create:all') &&
    user.permissions.includes('edit:all') &&
    user.permissions.includes('delete:all')
  )
}

const canCreate = (user: UserWithPermissions) => {
  return isAdminUser(user) || user.permissions.includes('create:all')
}

const canEdit = (user: UserWithPermissions) => {
  return isAdminUser(user) || user.permissions.includes('edit:own')
}

const canDelete = (user: UserWithPermissions) => {
  return isAdminUser(user) || user.permissions.includes('delete:own')
}

const rules = {
  isAuthenticatedUser: rule({ cache: 'contextual' })(
    async (parent, args, context: ContextModule) => {
      const user = await getUser(context)
      return Boolean(user)
    },
  ),

  isAdministratorUser: rule({ cache: 'contextual' })(
    async (parent, args, context: ContextModule) => {
      const user = await getUser(context)
      return isAdminUser(user)
    },
  ),

  canCreate: rule({ cache: 'contextual' })(
    async (parent, args, context: ContextModule) => {
      const user = await getUser(context)
      return canCreate(user)
    },
  ),

  canEdit: rule({ cache: 'contextual' })(
    async (parent, args, context: ContextModule) => {
      const user = await getUser(context)
      return canEdit(user)
    },
  ),

  canDelete: rule({ cache: 'contextual' })(
    async (parent, args, context: ContextModule) => {
      const user = await getUser(context)
      return canDelete(user)
    },
  ),

  // this still kind of needs some work - doesn't secure all cases
  isSelf: rule({ cache: 'strict' })(
    async (parent, args: Prisma.UserFindUniqueArgs, context: ContextModule) => {
      if (!args.where) {
        return true
      }

      const user = await getUser(context)

      let field: 'id' | 'email' = 'id'
      if (args.where.email) {
        field = 'email'
      }

      const targetUser = await context.prisma.user.findUnique({
        where: {
          [field]: args.where[field],
        },
      })

      return !!user && user.id === targetUser?.id
    },
  ),

  isCreatingAsSelf: rule({ cache: 'strict' })(
    async (
      parent,
      { data }: { data: Prisma.RecipeCreateInput },
      context: ContextModule,
    ) => {
      if (!data.author?.connect) {
        return false
      }

      const user = await getUser(context)
      if (!canCreate(user)) {
        return false
      }

      let field: 'id' | 'email' = 'id'
      if (data.author.connect.email) {
        field = 'email'
      }

      const userUser = await context.prisma.user.findUnique({
        where: {
          [field]: data.author.connect[field],
        },
      })

      return user?.id === userUser?.id
    },
  ),

  isRecipeOwner: rule({ cache: 'strict' })(
    async (
      parent,
      { where }: Prisma.RecipeFindUniqueArgs,
      context: ContextModule,
    ) => {
      const user = await getUser(context)
      const author = await context.prisma.recipe
        .findUnique({
          where,
        })
        .author()
      return !!user && user.id === author?.id
    },
  ),

  isRecipeSectionOwner: rule({ cache: 'strict' })(
    async (
      parent,
      { where }: Prisma.RecipeSectionFindUniqueArgs,
      context: ContextModule,
    ) => {
      const user = await getUser(context)
      const author = await context.prisma.recipeSection
        .findUnique({
          where,
        })
        .recipe()
        .author()
      return !!user && user.id === author?.id
    },
  ),

  isStepOwner: rule({ cache: 'strict' })(
    async (
      parent,
      { where }: Prisma.StepFindUniqueArgs,
      context: ContextModule,
    ) => {
      const user = await getUser(context)
      const author = await context.prisma.step
        .findUnique({
          where,
        })
        .recipeSection()
        .recipe()
        .author()
      return !!user && user.id === author?.id
    },
  ),

  isIngredientUnitOwner: rule({ cache: 'strict' })(
    async (
      parent,
      { where }: Prisma.IngredientUnitFindUniqueArgs,
      context: ContextModule,
    ) => {
      const user = await getUser(context)
      const author = await context.prisma.ingredientUnit
        .findUnique({
          where,
        })
        .recipeSection()
        .recipe()
        .author()
      return !!user && user.id === author?.id
    },
  ),
}

export const permissions = shield(
  {
    Query: {
      me: rules.isAuthenticatedUser,
    },
    User: {
      email: or(rules.isAdministratorUser, rules.isSelf),
    },
    Mutation: {
      createOneUser: rules.isAdministratorUser,
      updateOneUser: rules.isAdministratorUser,
      deleteOneUser: rules.isAdministratorUser,

      // XXX secure the create routes so that you can
      // only create and link under the right circumstances
      createOneIngredient: rules.canCreate,
      createOneRecipe: rules.isCreatingAsSelf,
      createOneRecipeSection: rules.canCreate,
      createOneStep: rules.canCreate,
      createOneTag: rules.canCreate,
      createOneIngredientUnit: rules.canCreate,
      createOneUnitSize: rules.isAdministratorUser,

      updateOneRecipe: or(
        rules.isAdministratorUser,
        and(rules.isRecipeOwner, rules.canEdit),
      ),
      updateOneIngredientUnit: or(
        rules.isAdministratorUser,
        and(rules.isIngredientUnitOwner, rules.canEdit),
      ),
      updateOneStep: or(
        rules.isAdministratorUser,
        and(rules.isStepOwner, rules.canEdit),
      ),
      updateOneRecipeSection: or(
        rules.isAdministratorUser,
        and(rules.isRecipeSectionOwner, rules.canEdit),
      ),
      updateOneIngredient: rules.isAdministratorUser,
      updateOneUnitSize: rules.isAdministratorUser,
      updateOneTag: rules.isAdministratorUser,

      deleteOneRecipe: or(
        rules.isAdministratorUser,
        and(rules.isRecipeOwner, rules.canDelete),
      ),
      deleteOneIngredientUnit: or(
        rules.isAdministratorUser,
        and(rules.isIngredientUnitOwner, rules.canDelete),
      ),
      deleteOneRecipeSection: or(
        rules.isAdministratorUser,
        and(rules.isRecipeSectionOwner, rules.canDelete),
      ),
      deleteOneStep: or(
        rules.isAdministratorUser,
        and(rules.isStepOwner, rules.canDelete),
      ),
      deleteOneIngredient: rules.isAdministratorUser,
      deleteOneTag: rules.isAdministratorUser,
      deleteOneUnitSize: rules.isAdministratorUser,
    },
  },
  {
    allowExternalErrors: true,
  },
)
