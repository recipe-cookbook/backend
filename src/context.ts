import { PrismaClient } from '@prisma/client'
import * as express from 'express'
import pino from 'pino'
import { Token } from './util/auth'

export interface ContextModule {
  prisma: PrismaClient
  log: pino.Logger
  request: express.Request & { user?: Token }
  response: express.Response
}
