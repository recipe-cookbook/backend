'use strict'
// require('make-promises-safe')
import * as Sentry from '@sentry/node'
import { ApolloServer } from 'apollo-server-express'
import cors from 'cors'
import createExpress, { ErrorRequestHandler } from 'express'
import { applyMiddleware } from 'graphql-middleware'
import pinoHttp from 'pino-http'
import { permissions } from './permissions'
import { prisma } from './prisma'
import { schema } from './schema'
import { authMiddleware } from './util/auth'
import fileUpload from 'express-fileupload'
import {
  getFileHandler,
  deleteFileHandler,
  postFileHandler,
} from './util/fileHandler'
import { getLogger, logger } from './util/logger'

// Error catching
if (process.env.SENTRY_DSN) {
  Sentry.init({
    dsn: process.env.SENTRY_DSN,
    environment: process.env.NODE_ENV,
  })
}

// Server
const express = createExpress()

express.use(
  pinoHttp({
    logger: logger as any, // workaround for type differences
  }),
)

express.get('/healthcheck', (req, res) => {
  res.status(200).json({ ok: true })
})

// express.get('/', (req, res) => {
//   res.status(200).redirect('/graphql')
// })

const errorRequestHandler: ErrorRequestHandler = (err, _req, res, next) => {
  if (!err) {
    return next()
  }
  res.err = err
  if (res.status) {
    res.status(500).send('Internal Server Error')
  } else {
    res.send('Internal Server Error')
  }
}
express.use(errorRequestHandler)

// Cors
const corsWhitelist = process.env.CORS_ORIGINS?.split('\n').map((s) =>
  s.startsWith('/') ? new RegExp(s.substring(1, s.length - 1)) : s,
)

express.use(
  cors({
    origin: corsWhitelist,
    methods: ['GET', 'HEAD', 'POST', 'PUT'],
    preflightContinue: false,
    optionsSuccessStatus: 200,
  }),
)

express.use(
  fileUpload({
    useTempFiles: false,
    limits: { fileSize: 5 * 1024 * 1024 },
    // tempFileDir: '/tmp/',
  }),
)

express.get('/file/:fileId', getFileHandler)
express.delete('/file/:fileId', deleteFileHandler)

// everything after this middleware requires auth
express.use(authMiddleware)
express.post('/recipe/:recipeId/file', postFileHandler)
express.put('/file/:fileId', postFileHandler)

// Apollo
const requestLogger = getLogger({ subsystem: 'apollo-request' })
const apollo = new ApolloServer({
  schema: applyMiddleware(schema, permissions),
  context: (expressContext) => {
    return {
      prisma: prisma,
      log: requestLogger,
      request: expressContext.req,
      response: expressContext.res,
    }
  },
})

apollo.start().then(() => {
  apollo.applyMiddleware({
    app: express,
    cors: {
      origin: corsWhitelist,
      methods: ['GET', 'HEAD', 'POST'],
      preflightContinue: false,
      optionsSuccessStatus: 200,
    },
  })

  express.listen(4000, '0.0.0.0', () => {
    logger.debug(`🚀 GraphQL service ready at http://0.0.0.0:4000/graphql`)
  })
})
