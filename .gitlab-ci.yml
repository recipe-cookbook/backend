variables:
  CI_BUILD_ARCHS: 'linux/arm64/v8'
  CI_BUILD_IMAGE: 'registry.gitlab.com/npappas/docker-buildx-qemu'
  SAST_ANALYZER_IMAGE_PREFIX: 'registry.gitlab.com/gitlab-org/security-products/analyzers'
  SAST_DEFAULT_ANALYZERS: 'nodejs-scan, eslint, tslint, secrets, kubesec'
  SAST_ANALYZER_IMAGE_TAG: 2
  SAST_DISABLE_DIND: 'false'
  SCAN_KUBERNETES_MANIFESTS: 'true'
  CS_MAJOR_VERSION: 2
  DS_ANALYZER_IMAGE_PREFIX: 'registry.gitlab.com/gitlab-org/security-products/analyzers'
  DS_DEFAULT_ANALYZERS: 'retire.js'
  DS_MAJOR_VERSION: 2
  DS_DISABLE_DIND: 'false'

include:
  - template: Container-Scanning.gitlab-ci.yml
  - template: Dependency-Scanning.gitlab-ci.yml
  - template: SAST.gitlab-ci.yml
  - template: Jobs/Code-Quality.gitlab-ci.yml

build_test:
  stage: build
  image: $CI_BUILD_IMAGE
  services:
    - name: docker:dind
      entrypoint: ["env", "-u", "DOCKER_HOST"]
      command: ["dockerd-entrypoint.sh"]
  variables:
    DOCKER_HOST: tcp://docker:2375/
    DOCKER_DRIVER: overlay2
    # See https://github.com/docker-library/docker/pull/166
    DOCKER_TLS_CERTDIR: ""
    IMAGE_TAG: $CI_REGISTRY_IMAGE/$CI_COMMIT_REF_SLUG:$CI_COMMIT_SHA
  retry: 2
  before_script:
    - echo "$CI_REGISTRY_PASSWORD" | docker login -u "$CI_REGISTRY_USER" --password-stdin $CI_REGISTRY
    # Use docker-container driver to allow useful features (push/multi-platform)
    - update-binfmts --enable # Important: Ensures execution of other binary formats is enabled in the kernel
    - docker buildx create --driver docker-container --use
    - docker buildx inspect --bootstrap
  script:
    - docker buildx build -f Dockerfile --platform $CI_BUILD_ARCHS --progress plain --pull -t $IMAGE_TAG --push .
  except:
    - master

build_prod:
  stage: build
  image: $CI_BUILD_IMAGE
  services:
    - name: docker:dind
      entrypoint: ["env", "-u", "DOCKER_HOST"]
      command: ["dockerd-entrypoint.sh"]
  variables:
    DOCKER_HOST: tcp://docker:2375/
    DOCKER_DRIVER: overlay2
    # See https://github.com/docker-library/docker/pull/166
    DOCKER_TLS_CERTDIR: ""
    IMAGE_TAG: $CI_REGISTRY_IMAGE/$CI_COMMIT_REF_SLUG:$CI_COMMIT_SHA
  retry: 2
  before_script:
    - echo "$CI_REGISTRY_PASSWORD" | docker login -u "$CI_REGISTRY_USER" --password-stdin $CI_REGISTRY
    - echo "$DOCKER_REGISTRY_PASSWORD" | docker login -u "$DOCKER_REGISTRY_USER" --password-stdin $DOCKER_REGISTRY
    # Use docker-container driver to allow useful features (push/multi-platform)
    - update-binfmts --enable # Important: Ensures execution of other binary formats is enabled in the kernel
    - docker buildx create --driver docker-container --use
    - docker buildx inspect --bootstrap
  script:
    - docker buildx build -f Dockerfile --platform $CI_BUILD_ARCHS --progress plain --pull -t $IMAGE_TAG -t "$DOCKER_REGISTRY_USER/cookbook-backend:latest" -t "$DOCKER_REGISTRY_USER/cookbook-backend:$CI_COMMIT_SHORT_SHA" --push .
  only:
    - master

deploy_production:
  image:
    # name: radicand/helm-arm64:latest
    name: alpine/helm:3.5.4
    entrypoint: ["sh", "-c"]
  stage: deploy
  environment:
    name: production
    url: https://cookbook-backend-$CI_ENVIRONMENT_SLUG.$KUBE_INGRESS_BASE_DOMAIN
    kubernetes:
      namespace: cookbook
  script:
    - helm upgrade cookbook-backend-$CI_ENVIRONMENT_SLUG ./manifests/cookbook-backend --install -n $KUBE_NAMESPACE --set image.repository=$CI_REGISTRY_IMAGE/$CI_COMMIT_REF_SLUG --set image.tag=$CI_COMMIT_SHA --set databaseUrl=$DATABASE_URL --set jwt.iss=$JWT_ISS --set jwt.aud=$JWT_AUD --set ingress.hosts[0].host=cookbook-backend-$CI_ENVIRONMENT_SLUG.$KUBE_INGRESS_BASE_DOMAIN --set ingress.hosts[0].paths[0]="/" --set CI_PROJECT_PATH_SLUG=$CI_PROJECT_PATH_SLUG --set CI_ENVIRONMENT_SLUG=$CI_ENVIRONMENT_SLUG --set corsOrigins="$CORS_ORIGINS" --set SENTRY_DSN="$SENTRY_DSN"
  only:
    - master

deploy_test:
  stage: deploy
  image:
    # name: radicand/helm-arm64:latest
    name: alpine/helm:3.5.4
    entrypoint: ["sh", "-c"]
  environment:
    name: test/$CI_COMMIT_BRANCH
    url: https://cookbook-backend-$CI_ENVIRONMENT_SLUG.$KUBE_INGRESS_BASE_DOMAIN
    kubernetes:
      namespace: cookbook
    on_stop: stop_test
  script:
    - helm upgrade cookbook-backend-$CI_ENVIRONMENT_SLUG ./manifests/cookbook-backend --install -n $KUBE_NAMESPACE --set image.repository=$CI_REGISTRY_IMAGE/$CI_COMMIT_REF_SLUG --set image.tag=$CI_COMMIT_SHA --set databaseUrl=$DATABASE_URL --set jwt.iss=$JWT_ISS --set jwt.aud=$JWT_AUD --set ingress.hosts[0].host=cookbook-backend-$CI_ENVIRONMENT_SLUG.$KUBE_INGRESS_BASE_DOMAIN --set ingress.hosts[0].paths[0]="/" --set CI_PROJECT_PATH_SLUG=$CI_PROJECT_PATH_SLUG --set CI_ENVIRONMENT_SLUG=$CI_ENVIRONMENT_SLUG --set corsOrigins="http://localhost:3000\nhttps://cookbook-frontend-$CI_ENVIRONMENT_SLUG.$KUBE_INGRESS_BASE_DOMAIN" --set SENTRY_DSN="$SENTRY_DSN"
  except:
    - master
  only:
    - branches

stop_test:
  stage: deploy
  image:
    # name: radicand/helm-arm64:latest
    name: alpine/helm:3.5.4
    entrypoint: ["sh", "-c"]
  environment:
    name: test/$CI_COMMIT_BRANCH
    url: https://cookbook-backend-$CI_ENVIRONMENT_SLUG.$KUBE_INGRESS_BASE_DOMAIN
    action: stop
    kubernetes:
      namespace: cookbook
  variables:
    GIT_STRATEGY: none
  script:
    - helm delete cookbook-backend-$CI_ENVIRONMENT_SLUG -n $KUBE_NAMESPACE
  when: manual
  except:
    - master
  only:
    - branches
