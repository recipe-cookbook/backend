/*
  Warnings:

  - The migration will add a unique constraint covering the columns `[recipeSectionId,order]` on the table `IngredientUnit`. If there are existing duplicate values, the migration will fail.
  - The migration will add a unique constraint covering the columns `[recipeId,order]` on the table `RecipeSection`. If there are existing duplicate values, the migration will fail.

*/
-- AlterTable
CREATE SEQUENCE "ingredientunit_order_seq";
ALTER TABLE "IngredientUnit" ALTER COLUMN "order" SET DEFAULT nextval('ingredientunit_order_seq');
ALTER SEQUENCE "ingredientunit_order_seq" OWNED BY "public"."IngredientUnit"."order";

-- AlterTable
ALTER TABLE "RecipeSection" ADD COLUMN     "order" SERIAL NOT NULL;

-- CreateIndex
CREATE UNIQUE INDEX "IngredientUnit.recipeSectionId_order_unique" ON "IngredientUnit"("recipeSectionId", "order");

-- CreateIndex
CREATE UNIQUE INDEX "RecipeSection.recipeId_order_unique" ON "RecipeSection"("recipeId", "order");
