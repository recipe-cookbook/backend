-- CreateIndex
ALTER TABLE "Ingredient"
ADD CONSTRAINT "Ingredient_name_unique" UNIQUE ("name");
ALTER TABLE "Ingredient"
DROP CONSTRAINT "Ingredient.name_unique";

-- CreateIndex
ALTER TABLE "JWTSub"
ADD CONSTRAINT "JWTSub_sub_unique" UNIQUE ("sub");
ALTER TABLE "Ingredient"
DROP CONSTRAINT "JWTSub.sub_unique";

-- CreateIndex
ALTER TABLE "Tag"
ADD CONSTRAINT "Tag_name_unique" UNIQUE ("name");
ALTER TABLE "Ingredient"
DROP CONSTRAINT "Tag.name_unique";

-- CreateIndex
ALTER TABLE "UnitSize"
ADD CONSTRAINT "UnitSize_name_unique" UNIQUE ("name");
ALTER TABLE "Ingredient"
DROP CONSTRAINT "UnitSize.name_unique";

-- CreateIndex
ALTER TABLE "User"
ADD CONSTRAINT "User_email_unique" UNIQUE ("email");
ALTER TABLE "Ingredient"
DROP CONSTRAINT "User.email_unique";