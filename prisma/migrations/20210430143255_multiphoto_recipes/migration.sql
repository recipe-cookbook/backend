/*
  Warnings:

  - You are about to drop the column `fileId` on the `Recipe` table. All the data in the column will be lost.
  - Added the required column `recipeId` to the `File` table without a default value. This is not possible if the table is not empty.

*/
-- DropForeignKey
ALTER TABLE "Recipe" DROP CONSTRAINT "Recipe_fileId_fkey";

-- AlterTable
ALTER TABLE "File" ADD COLUMN     "recipeId" TEXT NOT NULL;

-- AlterTable
ALTER TABLE "Recipe" DROP COLUMN "fileId";

-- AddForeignKey
ALTER TABLE "File" ADD FOREIGN KEY ("recipeId") REFERENCES "Recipe"("id") ON DELETE CASCADE ON UPDATE CASCADE;
