/*
  Warnings:

  - The migration will add a unique constraint covering the columns `[recipeSectionId,order]` on the table `Step`. If there are existing duplicate values, the migration will fail.

*/
-- AlterTable
CREATE SEQUENCE "step_order_seq";
ALTER TABLE "Step" ALTER COLUMN "order" SET DEFAULT nextval('step_order_seq');
ALTER SEQUENCE "step_order_seq" OWNED BY "public"."Step"."order";

-- CreateIndex
CREATE UNIQUE INDEX "Step.recipeSectionId_order_unique" ON "Step"("recipeSectionId", "order");
