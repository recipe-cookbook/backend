/*
  Warnings:

  - You are about to drop the column `recipeId` on the `File` table. All the data in the column will be lost.
  - Added the required column `userId` to the `File` table without a default value. This is not possible if the table is not empty.
  - Changed the type of `blob` on the `File` table. No cast exists, the column would be dropped and recreated, which cannot be done if there is data, since the column is required.

*/
-- DropForeignKey
ALTER TABLE "File" DROP CONSTRAINT "File_recipeId_fkey";

-- DropIndex
DROP INDEX "File_recipeId_unique";

-- AlterTable
ALTER TABLE "File" DROP COLUMN "recipeId",
ADD COLUMN     "userId" TEXT NOT NULL,
DROP COLUMN "blob",
ADD COLUMN     "blob" BYTEA NOT NULL;

-- AlterTable
ALTER TABLE "Recipe" ADD COLUMN     "fileId" TEXT;

-- AlterTable
ALTER TABLE "Step" ADD COLUMN     "fileId" TEXT;

-- AddForeignKey
ALTER TABLE "File" ADD FOREIGN KEY ("userId") REFERENCES "User"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "Recipe" ADD FOREIGN KEY ("fileId") REFERENCES "File"("id") ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "Step" ADD FOREIGN KEY ("fileId") REFERENCES "File"("id") ON DELETE SET NULL ON UPDATE CASCADE;
