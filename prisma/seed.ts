import { Prisma } from '@prisma/client'
import fs from 'fs'
import { prisma } from '../src/prisma'

async function main() {
  if ((await prisma.user.count().catch((ex) => 0)) > 0) {
    console.log(`Not seeding the database - users table already has data.`)
    return
  }

  const toast = fs.readFileSync('./prisma/toast.jpg')

  const email = process.env.EMAIL
  const name = process.env.NAME

  if (!email || !name) {
    console.log(
      `Please set the EMAIL and NAME environmental variables to properly bootstrap the database.`,
    )
    return
  }

  const user1 = await prisma.user.upsert({
    where: {
      email,
    },
    create: {
      email,
      name,
    },
    update: {
      name,
    },
  })
  console.log({ user1 })

  const UNIT_SIZES = [
    {
      name: 'ml',
      plural: 'ml',
      unitType: 'METRIC',
    },
    {
      name: 'g',
      plural: 'g',
      unitType: 'METRIC',
    },
    {
      name: 'whole',
      plural: 'whole',
      unitType: 'IMPERIAL',
    },
    {
      name: 'tsp',
      plural: 'tsps',
      unitType: 'IMPERIAL',
    },
    {
      name: 'tbsp',
      plural: 'tbsps',
      unitType: 'IMPERIAL',
    },
    {
      name: 'cup',
      plural: 'cups',
      unitType: 'IMPERIAL',
    },
    {
      name: 'oz',
      plural: 'oz',
      unitType: 'IMPERIAL',
    },
    {
      name: 'lb',
      plural: 'lb',
      unitType: 'IMPERIAL',
    },
    {
      name: 'loaf',
      plural: 'loaves',
      unitType: 'IMPERIAL',
    },
    {
      name: ' ',
      plural: ' ',
      unitType: 'IMPERIAL',
    },
  ]

  const upsertUnitSizes = async (unit: typeof UNIT_SIZES[0]) => {
    await prisma.unitSize.upsert({
      where: {
        name: unit.name,
      },
      create: unit as Prisma.UnitSizeUpsertArgs['create'],
      update: unit as Prisma.UnitSizeUpsertArgs['update'],
    })
  }

  for (let i = 0; i < UNIT_SIZES.length; i++) {
    await upsertUnitSizes(UNIT_SIZES[i])
  }

  const INGREDIENTS = [
    {
      name: '',
    },
  ]

  const upsertIngredients = async (item: typeof INGREDIENTS[0]) => {
    await prisma.ingredient.upsert({
      where: {
        name: item.name,
      },
      create: item,
      update: item,
    })
  }

  for (let i = 0; i < INGREDIENTS.length; i++) {
    await upsertIngredients(INGREDIENTS[i])
  }

  const TAGS: Prisma.TagCreateArgs['data'][] = [
    {
      name: 'breakfast',
    },
    {
      name: 'lunch',
    },
    {
      name: 'dinner',
    },
    {
      name: 'snack',
    },
    {
      name: 'side',
    },
  ]

  const upsertTags = async (item: typeof TAGS[0]) => {
    await prisma.tag.upsert({
      where: {
        name: item.name,
      },
      create: item,
      update: {
        name: item.name,
      },
    })
  }

  for (let i = 0; i < TAGS.length; i++) {
    await upsertTags(TAGS[i])
  }

  const recipe1 = await prisma.recipe.create({
    data: {
      title: 'test recipe',
      description: 'test description',
      author: {
        connect: {
          id: user1.id,
        },
      },
      notes: 'Notes here',
      source: 'Copied from XXXX',
      tags: {
        connect: {
          name: 'breakfast',
        },
      },
      sections: {
        create: {
          cookTimeMinutes: 10,
          prepTimeMinutes: 25,
          name: 'Sauce',
          servings: 5,
          servingUnit: 'spoonfuls',
          description: "This is a great tasting sauce you'll want to eat",
          ingredients: {
            create: [
              {
                unit: {
                  connect: {
                    name: 'ml',
                  },
                },
                amount: 200,
                notes: 'purified',
                ingredient: {
                  create: {
                    name: 'water',
                  },
                },
              },
              {
                unit: {
                  connect: {
                    name: 'g',
                  },
                },
                amount: 200,
                ingredient: {
                  create: {
                    name: 'sugar',
                  },
                },
              },
            ],
          },
          steps: {
            create: [
              {
                description: 'boil the water',
              },
              {
                description: 'filter the water',
              },
            ],
          },
        },
      },
    },
  })

  const photo1 = await prisma.file.create({
    data: {
      blob: toast,
      mimetype: 'image/jpeg',
      owner: {
        connect: {
          id: user1.id,
        },
      },
      recipe: {
        connect: {
          id: recipe1.id,
        },
      },
    },
  })

  const updatedRecipe = await prisma.recipe.update({
    data: {
      photo: {
        connect: {
          id: photo1.id,
        },
      },
    },
    where: {
      id: recipe1.id,
    },
  })

  console.log({ updatedRecipe })
}

main().finally(async () => {
  await prisma.$disconnect()
})
