# Cookbook backend

## Local dev

`docker run -d --name psql -p 5432:5432 -e POSTGRES_USER=cookbook -e POSTGRES_PASSWORD=localdev -e POSTGRES_DB=cookbook postgres:alpine`

`npx prisma migrate deploy --preview-feature`

`NAME="" EMAIL="" npm run seed`
