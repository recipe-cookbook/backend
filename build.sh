#!/bin/bash
BUILD_ID=`git rev-parse --short HEAD`

docker buildx build --platform linux/arm64/v8,linux/amd64 --pull -f Dockerfile -t radicand/cookbook-backend:latest -t radicand/cookbook-backend:$BUILD_ID .
# docker buildx build --platform linux/arm64/v8,linux/amd64 --pull -f Dockerfile -t radicand/cookbook-backend:latest -t radicand/cookbook-backend:$BUILD_ID --push .
