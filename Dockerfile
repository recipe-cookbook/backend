FROM ubuntu:20.04 as assets

RUN apt update && apt install -y curl && curl -fsSL https://deb.nodesource.com/setup_16.x | bash -
RUN DEBIAN_FRONTEND=noninteractive apt update && apt install -y --no-install-recommends openssl nodejs

RUN groupadd --gid 1000 node \
    && useradd --uid 1000 --gid node --shell /bin/bash --create-home node

ENV NODE_VERSION 16.0.0

USER 1000
WORKDIR /app

ENV DATABASE_URL="postgres://postgres:example@localhost/postgres"

COPY ./ ./

RUN npm ci
RUN npm run build

ENTRYPOINT [ "sh", "./entrypoint.sh" ]
